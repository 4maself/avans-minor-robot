*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    0
${TEST_URL}          http://cursusclient-kzastudent-0${GROUP_NR}.online.hotcontainers.nl/cursussen
${GROUP_NR}          4

${CURSUS_NAAM}       Ger


*** Test Cases ***
Opdracht 0: Google Test
    # Als deze test werkt dan werkt RobotFramework goed. Verwijder deze als je begint.
    Open Browser On Local Machine       https://www.google.com
    Wait Until Page Contains Element    name=q                    timeout=5

Opdracht 1: Login in de cursusclient
    Open Browser On Local Machine  ${TEST_URL}
    Wait Until Page Contains Element  id=login  timeout=5  error=None
    Click Element  id=login

Opdracht 2: Controleer of de pagina correct geladen is
    Wait Until Page Contains Element  id=create  timeout=5  error=None

Opdracht 3: Open het scherm om een nieuwe training aan te maken
    Click Element  id=create
    Wait Until Page Contains Element  id=addTeacher  timeout=5  error=None

Opdracht 4: Maak een nieuwe training aan
    Input Text  id=trainingName  ${CURSUS_NAAM}
    Select From List By Index  id=selectAttitude  1
    Select From List By Index  id=functieniveau  1
    Select From List By Index  id=passingCriteria  0
    Select From List By Index  id=status  0
    Input Text  id=maxParticipants  5
    Input Text  id=trainerName  Maurice
    Input Text  id=trainerEmail  maurice@maurice.nl
    Input Text  id=trainingDate  01-10-2019 18:00:00
    Input Text  id=trainingDescription  Wat heb je deze weken geleerd?
    Click Element  id=save
    Wait Until Page Contains Element  id=aanmelden  timeout=5  error=None

Opdracht 5: Meld je aan voor de training die je net hebt gemaakt
    Click Element  id=aanmelden
    Wait Until Page Contains Element  id=ja  timeout=5
    Sleep  .5  reason=Front end sucks
    Click Element  id=ja

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt
    Wait Until Page Contains  ${CURSUS_NAAM}  timeout=5  error=None
    Wait Until Page Contains Element  id=terug  timeout=5  error=None
    Click Element  id=terug  modifier=False
    

Opdracht 7: Controleer de details pagina van de aangemaakte training
    sleep  .5  reason=Front end still sucks
    Select Cursus From Cursuslist  ${CURSUS_NAAM}
    Click Cursus Details Button
    Wait Until Page Contains Element  id=terug  timeout=5  error=None
    Click Element  id=terug

Opdracht 8: Verwijder de aangemaakte training
    Select Cursus From Cursuslist  ${CURSUS_NAAM}
    Click Cursus Verwijderen Button

Opdracht 9: Test de logout functionaliteit
    Click Element  id=logout  modifier=False
    Wait Until Page Contains Element  id=login  timeout=5  error=None